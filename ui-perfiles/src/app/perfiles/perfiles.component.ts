import { Component, OnInit, Inject } from '@angular/core';
import { ApiPerfilesService } from '../api-perfiles.service';
import { Perfil } from '../../Models/Perfil';
import { Cliente } from '../../Models/Cliente';
import { MatDialog, MatDialogRef ,MAT_DIALOG_DATA} from '@angular/material';
import { Profile } from 'selenium-webdriver/firefox';
import { Area } from '../../Models/Area';
import { Modalidad } from '../../Models/Modalidad';
import { Carrera } from '../../Models/Carrera';
import { SubArea } from '../../Models/Subarea';
import { MensajeRealizadoComponent } from '../mensaje-realizado/mensaje-realizado.component';
import { Tutores } from 'src/Models/Tutores';
export interface DialogData{
  email:string;
  password:string;
}

@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.component.html'
})
export class PerfilesComponent implements OnInit {
  resultado:any;
  perfiles:Perfil[];
  perfil:Perfil;
  usuarios:Cliente[];
  usuario:Cliente;
  estado:boolean;
  animal:string;
  name:string;
  areas:Area[];
  area:Area;
  modalidades:Modalidad[];
  modalidad:Modalidad;
  carreras:Carrera[];
  carrera:Carrera;
  subareas:SubArea[];
  subarea:SubArea;
  tutores:Tutores[];
  constructor(private apiRes: ApiPerfilesService,public dialog:MatDialog,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
    this.setUsuarios();
    this.setPerfiles();
    this.estado=false;
    this.setAreas();
    this.setCareers();
    this.setModalidades();
    this.setSubAreas();
    this.setTutores();
  }
  setCareers(){
    this.apiRes.getCarreras().subscribe(
      result => {
        this.carreras=result.data;
      });
    return this.carreras;
  }  
  setAreas(){
    this.apiRes.getAreas().subscribe(
      result => {
        this.areas=result.data;
      });
    return this.areas;
  }  
  setSubAreas(){
    this.apiRes.getSubAreas().subscribe(
      result => {
        this.subareas=result.data;
      });
    return this.subareas;
  }  
  setModalidades(){
    this.apiRes.getModalidades().subscribe(
      result => {
        this.modalidades=result.data;
      });
    return this.modalidades;
  }  
  setPerfiles(){
    this.apiRes.getPerfiles().subscribe(
      result => {
        this.perfiles=result.data;
      });
    return this.perfiles;
  }
  setUsuarios(){
    this.apiRes.getUsuarios().subscribe(
      result => {
        this.usuarios=result.data;
      });
    return this.usuarios;
  }
  buscar_estudiantes(indice:number){
    this.usuario=null;
    while (this.usuario==null) {
      for (let index = 0; index < this.usuarios.length; index++) {
        if(this.usuarios[index].id==indice)
          this.usuario=this.usuarios[index];
      }
    }
    console.log(this.usuario);
    return this.usuario;
      
     }

openDialog(indice:number): void {
  this.usuario=this.buscar_estudiantes(indice);
  const dialogRef = this.dialog.open(PerfilesComponentDialog, {
    
    width: '580px',
    data: {usuario:this.usuario}
  });

}
openDialogPerfil(perfil:Perfil){
  var findmodalidad:Modalidad=this.buscar_modalidad(perfil.idModalidad);
  var findarea:Area=this.buscar_area(perfil.idArea);
  var findsubarea:SubArea=this.buscar_subarea(perfil.idSubarea);
  var findcarrera:Carrera=this.buscar_carrera(perfil.idCarrera);
  const dialogRef = this.dialog.open(PerfilesComponentDialogCompleto, {
  
    width: '650px',
    data: {perfil:perfil,
     modalidad:findmodalidad,
     area:findarea,
     subarea:findsubarea,
     carrera:findcarrera
    }
  });
}
buscar_modalidad(id:number){
  this.modalidad=null;
  while (this.modalidad==null) {
    for (let index = 0; index < this.modalidades.length; index++) {
      if(this.modalidades[index].id==id)
        this.modalidad=this.modalidades[index];
    }}return this.modalidad;
}
buscar_area(id:number){
  this.area=null;
  while (this.area==null) {
    for (let index = 0; index < this.areas.length; index++) {
      if(this.areas[index].id==id)
        this.area=this.areas[index];
    }}return this.area;
}
buscar_subarea(id:number){
  this.subarea=null;
  while (this.subarea==null) {
    for (let index = 0; index < this.subareas.length; index++) {
      if(this.subareas[index].id==id)
        this.subarea=this.subareas[index];
    }}return this.subarea;
}
buscar_carrera(id:number){
  this.carrera=null;
  while (this.carrera==null) {
    for (let index = 0; index < this.carreras.length; index++) {
      if(this.carreras[index].id==id)
        this.carrera=this.carreras[index];
    }}return this.carrera;
}

openDialogEdit(perfil:Perfil){
  const dialogRef = this.dialog.open(PerfilesComponentEdit, {
    
    width: '650px',
    data: {
      perfil:perfil,areas:this.areas,
      modalidades:this.modalidades,
      subareas:this.subareas,
      carreras:this.carreras,
      tutores:this.tutores
    }
  });
}

delete(perfil:Perfil){
  this.apiRes.deletePerfil(perfil).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
}
buscar(){
  let word=(<HTMLInputElement>document.getElementById("searchfield")).value;
  this.apiRes.searchprofile(word).subscribe(
    result => {
      this.perfiles=result.data;
    });
  return this.perfiles;
}
setTutores(){
  this.apiRes.getTutores().subscribe(
    result => {
      this.tutores=result.data;
    });
  return this.tutores;
}
}
//////////////////////////////////////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-perfiles-dialog',
  templateUrl: './perfiles.component.dialog.html',
})
export class PerfilesComponentDialog {

  constructor(
    public dialogRef: MatDialogRef<PerfilesComponentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
///////////////////////////////////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-perfiles-dialog-completo',
  templateUrl: './perfiles.component.dialog.completo.html',
})
export class PerfilesComponentDialogCompleto {

  constructor(
    public dialogRef: MatDialogRef<PerfilesComponentDialogCompleto>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
///////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-perfiles-edit',
  templateUrl: './perfiles.component.edit.html'
})
export class PerfilesComponentEdit {
  email:any;
  password:any;
  perfil:Profile;
  resultado:any;

  estado1:boolean;
  estado2:boolean;
  disableFlagContinuo:boolean=false;
  disableFlagCambio:boolean=false;
  idcarrera:number=null;
  idarea:number=null;
  idsubarea:number=null;
  idmodalidad:number=null;
  idtutor:number=null;

  constructor(
    public dialogRef: MatDialogRef<PerfilesComponentEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  carreraselect(event:any){
    this.idcarrera=event.value;
  }
  areaselect(event:any){
    this.idarea=event.value;
  }
  subareaselect(event:any){
    this.idsubarea=event.value;
  }
  modalidadselect(event:any){
    this.idmodalidad=event.value;
  }
  verificar(event:any){
    if (event.checked) {
      this.estado1=true;
     this.disableFlagCambio=true;
    }else {
      this.estado1=false;
      this.disableFlagCambio=false;
    }
  }
  verificar2(event:any){
    if (event.checked) {
      this.estado2=true;
     this.disableFlagContinuo=true;
    }else {this.estado2=false;
      this.disableFlagContinuo=false;
    }
}
  modificar(id:number){
    var tutor=this.idtutor;
    var continuo=this.estado1;
    var cambio=this.estado2;
    var carrera=this.idcarrera;
    var gestion=(<HTMLInputElement>document.getElementById("gestion")).textContent;
    var titulo=(<HTMLInputElement>document.getElementById("titulo")).value;
    var area=this.idarea;
    var subarea=this.idsubarea;
    var modalidad=this.idmodalidad
    var general=(<HTMLInputElement>document.getElementById("general")).value;
    var especificos=(<HTMLInputElement>document.getElementById("especificos")).value;
    var descripcion=(<HTMLInputElement>document.getElementById("descripcion")).value;
    var director=(<HTMLInputElement>document.getElementById("director")).value;
    var docente=(<HTMLInputElement>document.getElementById("docente")).value;
    this.apiRes.updatePerfil(id,
      tutor,continuo,cambio,carrera,gestion,titulo,area,subarea,modalidad,general,especificos
      ,descripcion,director,docente
      ).subscribe(
      result => {
       this.perfil=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
}   
tutorselect(event:any){
  this.idtutor=event.value;
}
}