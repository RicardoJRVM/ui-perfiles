import { Component } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
estado:boolean;
  constructor(public login:LoginComponent,public router:Router) {}
  title = 'app';
  ngOnInit() {
    this.estado=false;
    
    this.verificarEstado();
  }
  verificarEstado(){
    var log=localStorage.getItem("Logeado");
    if(log==="true"){
      this.estado=true;
    }
    else{
      if (log==="false") {
        this.estado=false;
      }
    }
  }

  eliminarsesion(){
    localStorage.clear();
    this.router.navigate(['login']);
    window.location.reload();
  }
}