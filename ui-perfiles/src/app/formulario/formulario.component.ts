import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiPerfilesService } from '../api-perfiles.service';
import { Carrera } from '../../Models/Carrera';
import { Area } from '../../Models/Area';
import { SubArea } from '../../Models/Subarea';
import { Modalidad } from '../../Models/Modalidad';
import { Cliente } from '../../Models/Cliente';
import { Perfil } from '../../Models/Perfil';
import { MensajeRealizadoComponent } from '../mensaje-realizado/mensaje-realizado.component';
import { Tutores } from 'src/Models/Tutores';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent {
  
  estudiantes:Cliente[];
  carreras:Carrera[];
  areas:Area[];
  subareas:SubArea[];
  modalidades:Modalidad[];
  estado1:boolean=false;
  estado2:boolean=false;
  disableFlagContinuo:boolean=false;
  disableFlagCambio:boolean=false;
  idcarrera:number=null;
  idarea:number=null;
  idsubarea:number=null;
  idmodalidad:number=null;
  idestudiante:number=null;
  idtutor:number=null;
  perfil:Perfil=null;
  tutores:Tutores[];

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver, private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}
  ngOnInit() {
    this.setEstudiantes();
    this.setCareers();
    this.setAreas();
    this.setSubAreas();
    this.setModalidades();
    this.setTutores();
  }
  setEstudiantes(){
    this.apiRes.getUsuarios().subscribe(
      result => {
        this.estudiantes=result.data;
      });
    return this.estudiantes;
  }
  setCareers(){
    this.apiRes.getCarreras().subscribe(
      result => {
        this.carreras=result.data;
      });
    return this.carreras;
  }  
  setAreas(){
    this.apiRes.getAreas().subscribe(
      result => {
        this.areas=result.data;
      });
    return this.areas;
  }  
  setSubAreas(){
    this.apiRes.getSubAreas().subscribe(
      result => {
        this.subareas=result.data;
      });
    return this.subareas;
  }  
  setModalidades(){
    this.apiRes.getModalidades().subscribe(
      result => {
        this.modalidades=result.data;
      });
    return this.modalidades;
  }  
  register(){
    var tutor=this.idtutor;
    var continuo=this.estado1;
    var cambio=this.estado2;
    var carrera=this.idcarrera;
    var gestion=(<HTMLInputElement>document.getElementById("gestion")).textContent;
    var titulo=(<HTMLInputElement>document.getElementById("titulo")).value;
    var area=this.idarea;
    var subarea=this.idsubarea;
    var modalidad=this.idmodalidad
    var general=(<HTMLInputElement>document.getElementById("general")).value;
    var especificos=(<HTMLInputElement>document.getElementById("especificos")).value;
    var descripcion=(<HTMLInputElement>document.getElementById("descripcion")).value;
    var director=(<HTMLInputElement>document.getElementById("director")).value;
    var docente=(<HTMLInputElement>document.getElementById("docente")).value;
    var user_id=this.idestudiante;
    this.apiRes.createPerfil(
      tutor,continuo,cambio,carrera,gestion,titulo,area,subarea,modalidad,general,especificos
      ,descripcion,director,docente,user_id
      ).subscribe(
      result => {
       this.perfil=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
  }
  verificar(event:any){
    if (event.checked) {
      this.estado1=true;
      this.disableFlagCambio=true;
    }else {
      this.estado1=false;
      this.disableFlagCambio=false;
    }
  }
  verificar2(event:any){
    if (event.checked) {
      this.estado2=true;
     this.disableFlagContinuo=true;
    }else {this.estado2=false;
      this.disableFlagContinuo=false;
    }
}
  carreraselect(event:any){
    this.idcarrera=event.value;
  }
  areaselect(event:any){
    this.idarea=event.value;
  }
  subareaselect(event:any){
    this.idsubarea=event.value;
  }
  modalidadselect(event:any){
    this.idmodalidad=event.value;
  }
  estudianteselect(event:any){
    this.idestudiante=event.value;
  }
  setTutores(){
    this.apiRes.getTutores().subscribe(
      result => {
        this.tutores=result.data;
      });
    return this.tutores;
  }
  tutorselect(event:any){
    this.idtutor=event.value;
  }
}
export class TabGroupDynamicHeightExample {}
export class ExpansionOverviewExample {
  panelOpenState = false;
}
