import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiPerfilesService } from '../api-perfiles.service';
import { Cliente } from 'src/Models/Cliente';
import { Router } from '@angular/router';
import { MensajeRealizadoComponent } from '../mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  usuarios:Cliente[];
  respuesta:any;
  errors:any;
  public logeado:boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  constructor(private breakpointObserver: BreakpointObserver,public apiRes:ApiPerfilesService,public router:Router,public error:MensajeRealizadoComponent) {}
  ngOnInit() {
    this.setUsers();
    this.logeado=false;
  }
  setUsers(){
    this.apiRes.getUsuarios().subscribe(
      result => {
        this.usuarios=result.data;
      });
    return this.usuarios;
  }    
  login(){
        let email=(<HTMLInputElement>document.getElementById("email")).value;
        let password=(<HTMLInputElement>document.getElementById("password")).value;
        
        this.apiRes.auth(email,password).then(
          result => {
            this.respuesta=result;
          }
        );
        setTimeout(() => {
          if(this.respuesta!=false){
            this.router.navigate(['home']);
            localStorage.setItem("Logeado","true");
            window.location.reload();
          }else  {
            this.error.openError();
            localStorage.setItem("Logeado","false");
            (<HTMLInputElement>document.getElementById('email')).value="";
            (<HTMLInputElement>document.getElementById('password')).value="";
          }
        }, 3000);
      }

      enviarLogeado(){
        //console.log(this.logeado)
        return this.logeado;
      }



  }
