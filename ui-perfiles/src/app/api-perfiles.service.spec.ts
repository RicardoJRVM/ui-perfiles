import { TestBed } from '@angular/core/testing';

import { ApiPerfilesService } from './api-perfiles.service';

describe('ApiPerfilesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiPerfilesService = TestBed.get(ApiPerfilesService);
    expect(service).toBeTruthy();
  });
});
