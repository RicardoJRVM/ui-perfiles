import { Component, OnInit } from '@angular/core';
import {ApiTutorsService} from "../api-tutor.service";
import { HttpClient } from '@angular/common/http';
import {Tutores} from "../../Models/Tutores";


@Component({
  selector: 'app-tutors',
  templateUrl: './tutors.component.html',
  styleUrls: ['./tutors.component.css']
})
export class TutorsComponent implements OnInit {
  tutors: Tutores[];
  tutor: Tutores;
  // tutors1: Tutores = {
  //   NameTutor: null,
  // };
  constructor(private tutorService: ApiTutorsService) {}


  
  ngOnInit() {
    this.setTutors();
    }
  setTutors() {
    this.tutorService.getTutors().subscribe(
      result => {
        this.tutors = result.data;
      });
    return this.tutors;
  }
}
