import { Component, OnInit } from '@angular/core';
import { ApiAreaService } from '../api-area.service';
import { HttpClient } from '@angular/common/http';
import { Areas } from 'src/Models/Areas';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.css']
})
export class AreasComponent implements OnInit {
  areas: Areas[];
  area: Areas;
  areas1: Areas = {
    area: null,
  };
  constructor(private areService: ApiAreaService) {}

  ngOnInit() {
    this.setAreas();
    }
  setAreas() {
    this.areService.getAreas().subscribe(
      result => {
        this.areas = result.data;
      });
    return this.areas;
  }
}
