import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders }from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Perfil } from '../Models/Perfil';
import { Modalidad } from '../Models/Modalidad';
import { Area } from '../Models/Area';
import {  Router } from '@angular/router';
import { Carrera } from '../Models/Carrera';
import { SubArea } from '../Models/Subarea';
import { Cliente } from '../Models/Cliente';
import { filter,map, catchError } from 'rxjs/operators';
import { MensajeRealizadoComponent } from './mensaje-realizado/mensaje-realizado.component';
import { TutorsComponent } from './tutores/tutors.component';
import { Tutores } from '../Models/Tutores';

@Injectable({
  providedIn: 'root'
})
export class ApiPerfilesService {
  public url:string;
  public id:number;
  public mensaje:any;
  constructor(public http: HttpClient) {
    this.url ="http://localhost:8000/api/";
   }
   getUsuarios():Observable<any>{
    return this.http.get(this.url+'clients');
   }
   getPerfiles():Observable<any>{
    return this.http.get(this.url+'profiles');
}
  getEstudiantes(indice: number): Observable<any>{
   return this.http.get(this.url+'profiles/'+indice+'/clients');
}
getUsuario(indice: number):Observable<any>{
    return this.http.get(this.url+'clients/'+indice);
 }
getCarreras():Observable<any>{
  return this.http.get(this.url+'careers');
 }
 getAreas():Observable<any>{
  return this.http.get(this.url+'areas');
 }
 getTutors():Observable<any>{
  return this.http.get(this.url+'tutors');
 }
 getSubAreas():Observable<any>{
  return this.http.get(this.url+'subareas');
 }
 getModalidades():Observable<any>{
  return this.http.get(this.url+'modalities');
 }
 deleteArea(area:Area):Observable<any>{
  let json =JSON.stringify(area);
  this.id=area.id;
  console.log(json);
  return this.http.delete(this.url+'areas/'+this.id);
 }
 deleteCarrera(carrera:Carrera):Observable<any>{
  let json =JSON.stringify(carrera);
  this.id=carrera.id;
  console.log(json);
  return this.http.delete(this.url+'careers/'+this.id);
 }
 deleteModalidad(modalidad:Modalidad):Observable<any>{
  let json =JSON.stringify(modalidad);
  this.id=modalidad.id;
  console.log(json);
  return this.http.delete(this.url+'modalities/'+this.id);
 }
 deleteSubarea(subarea:SubArea):Observable<any>{
  let json =JSON.stringify(subarea);
  this.id=subarea.id;
  console.log(json);
  return this.http.delete(this.url+'subareas/'+this.id);
 }
 deleteTutor(tutors:Tutores):Observable<any>{
  let json =JSON.stringify(tutors);
  this.id=tutors.id;
  console.log(json);
  return this.http.delete(this.url+'tutors/'+this.id);
 }
 deleteUsuario(cliente:Cliente):Observable<any>{
  let json =JSON.stringify(cliente);
  this.id=cliente.id;
  console.log(json);
  return this.http.delete(this.url+'clients/'+this.id);
 }

 updateArea(cambio:string,id:number):Observable<any>{
  var json ={
    NameArea:cambio
  }
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.patch(this.url+'areas/'+id, params, {headers: headers});
 }
 updateCarrera(cambio:string,id:number):Observable<any>{
  var json ={
    NameCareer:cambio
  }
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.patch(this.url+'careers/'+id, params, {headers: headers});
 }
 updateModalidad(cambio:string,id:number):Observable<any>{
  var json ={
    NameModality:cambio
  }
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.patch(this.url+'modalities/'+id, params, {headers: headers});
 }
 updateSubarea(cambio:string,id:number):Observable<any>{
  var json ={
    NameSubArea:cambio
  }
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.patch(this.url+'subareas/'+id, params, {headers: headers});
 }
 updateTutor(cambio:string,id:number):Observable<any>{
  var json ={
    nameTutors:cambio

  }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.patch(this.url+'tutors/'+id, params, {headers: headers});
 }

 updateUsuario(emailn:string,passwordn:string,namesn:string,psurnamen:string,
  msurnamen:string,phonen:string,id:number):Observable<any>{
  var json ={
    email:emailn,
    password:passwordn,
    names:namesn,
    psurname:psurnamen,
    msurname:msurnamen,
    phone:phonen
  }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.patch(this.url+'clients/'+id, params, {headers: headers});
 }

 createArea(name:string):Observable<any>{
  var json ={
    NameArea:name
  }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url+'areas', params, {headers: headers});
 }
 createCarrera(name:string):Observable<any>{
  var json ={
    NameCareer:name
  }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url+'careers', params, {headers: headers});
 }
 createModalidad(name:string):Observable<any>{
  var json ={
    NameModality:name
  }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url+'modalities', params, {headers: headers});
 }
 createSubarea(name:string):Observable<any>{
  var json ={
    NameSubArea:name
  }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url+'subareas', params, {headers: headers});
 }
 createTutor(name:string):Observable<any>{
  var json ={
    nameTutor:name
  }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url+'tutors', params, {headers: headers});
 }

 createUsuario(emailn:string,passwordn:string,namesn:string,psurnamen:string,
  msurnamen:string,phonen:string):Observable<any>{
    var json ={
      email:emailn,
      password:passwordn,
      names:namesn,
      psurname:psurnamen,
      msurname:msurnamen,
      phone:phonen
    }  
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url+'clients', params, {headers: headers});
 }
 createPerfil(
   tutorn:any,continuon:boolean,cambion:boolean,carreran:number,
   gestionn:string,titulon:string,arean:number,subarean:number,
   modalidadn:number,generaln:string,especificosn:string,descripcionn:string,
   directorn:string,docenten:string,user_idn:number
 ):Observable<any>{
    var json ={
      tutor:tutorn,
      gestion:gestionn,
      conjunto:continuon,
      cambio:cambion,
      titulo:titulon,
      obj_general:generaln,
      obj_especificos:especificosn,
      descripcion:descripcionn,
      director_carrera:directorn,
      docente_materia:docenten,
      client_id:user_idn,
      idArea:arean,
      idSubarea:subarean,
      idModalidad:modalidadn,
      idCarrera:carreran
    }
    console.log(json);
    let params =json;
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this.http.post(this.url+'profiles', params, {headers: headers});
 }
 updatePerfil(id:number,
  tutorn:any,continuon:boolean,cambion:boolean,carreran:number,
  gestionn:string,titulon:string,arean:number,subarean:number,
  modalidadn:number,generaln:string,especificosn:string,descripcionn:string,
  directorn:string,docenten:string
):Observable<any>{
   var json ={
     tutor:tutorn,
     gestion:gestionn,
     conjunto:continuon,
     cambio:cambion,
     titulo:titulon,
     obj_general:generaln,
     obj_especificos:especificosn,
     descripcion:descripcionn,
     director_carrera:directorn,
     docente_materia:docenten,
     idArea:arean,
     idSubarea:subarean,
     idModalidad:modalidadn,
     idCarrera:carreran
   }
   console.log(json);
   let params =json;
   let headers = new HttpHeaders().set('Content-Type','application/json');
   return this.http.patch(this.url+'profiles/'+id, params, {headers: headers});
}
 deletePerfil(perfil:Perfil):Observable<any>{
  let json =JSON.stringify(perfil);
  this.id=perfil.id;
  return this.http.delete(this.url+'profiles/'+this.id);
 }

 searchprofile(word:string):Observable<any>{
  var json ={
                data:word
            }
  let params =json;
  let headers = new HttpHeaders().set('Content-Type','application/json');
  return this.http.post(this.url+'searchprofile/', params, {headers: headers});
 }
 searchclient(word:string):Observable<any>{
  var json ={
                data:word
            }
  let params =json;
  let headers = new HttpHeaders().set('Content-Type','application/json');
  return this.http.post(this.url+'searchclient/', params, {headers: headers});
 }
 public auth(email:string,pass:string ){
  var json ={
    email:email,
    password:pass
  }
let params =json;
let headers = new HttpHeaders().set('Content-Type','application/json');
return this.http.post(this.url+'login/', params, {headers: headers}).toPromise()
// .then(()=>{
  
// })
.catch(error =>{
  return false;
});
 }
 getTutores():Observable<any>{
  return this.http.get(this.url+'tutors');
 }
}
