import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeRealizadoComponent } from './mensaje-realizado.component';

describe('MensajeRealizadoComponent', () => {
  let component: MensajeRealizadoComponent;
  let fixture: ComponentFixture<MensajeRealizadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeRealizadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeRealizadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
