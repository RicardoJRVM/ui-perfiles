import { Component, OnInit, Output, Inject } from '@angular/core';
import { MatSnackBar, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EventEmitter } from 'events';
import { DialogData } from '../usuarios/usuarios.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mensaje-realizado',
  templateUrl: './mensaje-realizado.component.html'
})
export class MensajeRealizadoComponent implements OnInit {
//@Output() emitEvent:EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(public snackBar: MatSnackBar,public dialog:MatDialog) { }

  ngOnInit() {
  }
  public openSnackBar() {
    this.snackBar.openFromComponent(DialogComponent, {
      duration: 1000,
    });
  }
  public openError():void{
    const dialogRef = this.dialog.open(ErrorDialogComponent, {
      width: '390px',
      data: {}
    });
  }
}

///////////////////////////////////////////////
@Component({
  selector: 'app-mensaje-realizado-component',
  templateUrl: './mensaje-realizado.dialog.component.html',
  styles: [`
    .example-pizza-party {
      color: #5dc1b9;
    }
  `],
})
export class DialogComponent {}
///////////////////////////////////////////////
@Component({
  selector: 'app-mensaje-error-component',
  templateUrl: './mensaje-error.dialog.component.html',
})
export class ErrorDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ErrorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,public router:Router) {}
    ngOnInit() {
    }
  cerrar(): void {
    this.dialogRef.close();
  }

}

