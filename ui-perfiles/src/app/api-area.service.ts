import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Areas } from '../Models/Areas';


@Injectable({
  providedIn: 'root'
})
export class ApiAreaService {
  public url: string;
  constructor(public http: HttpClient) {
    this.url = 'http://localhost:8000/api/';
   }
   getAreas(): Observable<any> {
    return this.http.get(this.url + 'areas');
   }
}
