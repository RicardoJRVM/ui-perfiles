import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule,
        MatTooltipModule, MatCardModule, MatInputModule, MatProgressSpinnerModule, MatTableModule, MatMenuModule, MatDialogModule, MatSnackBarModule,
        } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Route} from '@angular/router';
import { FormularioComponent } from './formulario/formulario.component';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { PerfilesComponent, PerfilesComponentDialog,PerfilesComponentDialogCompleto, PerfilesComponentEdit } from './perfiles/perfiles.component';
import { UsuariosComponent, UsuariosComponentDialog, UsuariosComponentEdit } from './usuarios/usuarios.component';
import { FormulariosModule } from './formularios/formularios.module';
import { FormularioModalidadComponent } from './formularios/formulario-modalidad/formulario-modalidad.component';
import { FormularioAreaComponent } from './formularios/formulario-area/formulario-area.component';
import { FormularioSubareaComponent } from './formularios/formulario-subarea/formulario-subarea.component';
import { FormularioCarreraComponent } from './formularios/formulario-carrera/formulario-carrera.component';
import { FormularioUsuarioComponent } from './formularios/formulario-usuario/formulario-usuario.component';
import { ListaAreasComponent, ListaAreasComponentEdit } from './listados/lista-areas/lista-areas.component';
import { ListaCarrerasComponent, ListaCarrerasComponentEdit } from './listados/lista-carreras/lista-carreras.component';
import { ListaModalidadesComponent, ListaModalidadesComponentEdit } from './listados/lista-modalidades/lista-modalidades.component';
import { ListaSubareasComponent, ListaSubareasComponentEdit } from './listados/lista-subareas/lista-subareas.component';
import { ListadosModule } from './listados/listados.module';
import { ApiPerfilesService } from './api-perfiles.service';
import { MensajeRealizadoComponent, DialogComponent, ErrorDialogComponent } from './mensaje-realizado/mensaje-realizado.component';
import {ListaTutorsComponent,ListaTutorsComponentEdit} from "./listados/lista-tutors/lista-tutors.component";
import {FormularioTutorComponent }from "./formularios/formulario-tutor/formulario-tutor.component";
const routes: Route[] = [
{ path:  '', redirectTo:'/login', pathMatch:'full'},
{ path: 'home',component: HomeComponent},
{ path: 'login', component: LoginComponent },
{ path: 'formulario', component: FormularioComponent},
{ path: 'profiles', component: PerfilesComponent},
{ path: 'users', component: UsuariosComponent},
{ path: 'inputmodalidad', component: FormularioModalidadComponent},
{ path: 'inputcarrera', component: FormularioCarreraComponent },
{ path: 'inputarea', component: FormularioAreaComponent},
{ path: 'inputsubarea', component: FormularioSubareaComponent},
{ path: 'inputusuario', component: FormularioUsuarioComponent},
{ path: 'areas', component: ListaAreasComponent},
{ path: 'carreras', component: ListaCarrerasComponent},
{ path: 'modalidades', component: ListaModalidadesComponent},
{ path: 'subareas', component: ListaSubareasComponent},
{ path: 'mensaje', component: MensajeRealizadoComponent},
{ path: 'tutors', component: ListaTutorsComponent},
{ path: 'inputtutors', component: FormularioTutorComponent},


];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    FormularioComponent,
    PerfilesComponent,
    UsuariosComponent,
    PerfilesComponentDialog,
    UsuariosComponentDialog,
    PerfilesComponentDialogCompleto,
    UsuariosComponentEdit,
    PerfilesComponentEdit,
    MensajeRealizadoComponent,
    DialogComponent,
    ErrorDialogComponent,
    FormularioTutorComponent,
    ListaTutorsComponentEdit,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSelectModule,
    MatCheckboxModule,
    MatExpansionModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormulariosModule,
    MatMenuModule,
    MatDialogModule,
    ListadosModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  exports:[AppComponent,
            MensajeRealizadoComponent,
          DialogComponent],
  providers: [ApiPerfilesService,MensajeRealizadoComponent,DialogComponent,ErrorDialogComponent,
              LoginComponent],
  bootstrap: [AppComponent],
  entryComponents:[PerfilesComponentDialog,UsuariosComponentDialog,
            PerfilesComponentDialogCompleto,
            ListaAreasComponentEdit, 
            ListaCarrerasComponentEdit,
            ListaModalidadesComponentEdit,
            ListaSubareasComponentEdit,
            UsuariosComponentEdit,
            ListaTutorsComponentEdit,
          PerfilesComponentEdit,
          DialogComponent,
          ErrorDialogComponent]
})
export class AppModule { }
