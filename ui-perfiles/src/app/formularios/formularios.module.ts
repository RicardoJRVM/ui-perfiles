import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioModalidadComponent } from './formulario-modalidad/formulario-modalidad.component';
import { MatToolbarModule, MatButtonModule, MatIconModule, MatExpansionModule, MatInputModule, MatListModule, MatCardModule } from '@angular/material';
import { FormularioCarreraComponent } from './formulario-carrera/formulario-carrera.component';
import { FormularioAreaComponent } from './formulario-area/formulario-area.component';
import { FormularioSubareaComponent } from './formulario-subarea/formulario-subarea.component';
import { FormularioUsuarioComponent } from './formulario-usuario/formulario-usuario.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatExpansionModule,
    MatInputModule,
    MatListModule,
    MatCardModule,
    ReactiveFormsModule,
    FormsModule
    
  ],
  declarations: [FormularioModalidadComponent, FormularioCarreraComponent, FormularioAreaComponent, FormularioSubareaComponent, FormularioUsuarioComponent],
  exports:[FormularioModalidadComponent,
    FormularioAreaComponent,
    FormularioCarreraComponent,
    FormularioSubareaComponent,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatExpansionModule,
    MatInputModule,
    MatListModule,
    MatCardModule
]

})
export class FormulariosModule { }
