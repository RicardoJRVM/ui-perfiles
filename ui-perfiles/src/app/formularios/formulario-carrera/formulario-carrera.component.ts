import { Component, OnInit } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { MensajeRealizadoComponent } from '../../mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-formulario-carrera',
  templateUrl: './formulario-carrera.component.html'
})
export class FormularioCarreraComponent implements OnInit {
  Name:string;
  resultado:any;
  constructor(private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
  }
  ingresar(){
    var texto=(<HTMLInputElement>document.getElementById("NameCareer")).value;
    this.Name=texto;
    this.apiRes.createCarrera(this.Name).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
   }
}
