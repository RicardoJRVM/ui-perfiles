import { Component, OnInit } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { MensajeRealizadoComponent } from '../../mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-formulario-tutor',
  templateUrl: './formulario-tutor.component.html'
})
export class FormularioTutorComponent implements OnInit {
  Name:string;
  resultado:any;
  constructor(private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
  }
  ingresar(){
    var texto=(<HTMLInputElement>document.getElementById("nameTutors")).value;
    this.Name=texto;
    console.log(this.Name);
    this.apiRes.createTutor(this.Name).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
   }
}
