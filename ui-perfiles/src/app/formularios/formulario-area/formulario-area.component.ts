import { Component, OnInit } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { Router } from '@angular/router';
import { MensajeRealizadoComponent } from '../../mensaje-realizado/mensaje-realizado.component';
@Component({
  selector: 'app-formulario-area',
  templateUrl: './formulario-area.component.html'
})
export class FormularioAreaComponent implements OnInit {
  Name:string;
  resultado:any;
  constructor(private apiRes: ApiPerfilesService,public router:Router,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
  }
 ingresar(){
  var texto=(<HTMLInputElement>document.getElementById("NameArea")).value;
  this.Name=texto;
  this.apiRes.createArea(this.Name).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
 }

 verificar(event:any){
  var element=<HTMLInputElement>document.getElementById('submitbut');element.disabled=false;
if(event=="")
{
  var element=<HTMLInputElement>document.getElementById('submitbut');element.disabled=true;
}
}
}
