import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioModalidadComponent } from './formulario-modalidad.component';

describe('FormularioModalidadComponent', () => {
  let component: FormularioModalidadComponent;
  let fixture: ComponentFixture<FormularioModalidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioModalidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioModalidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
