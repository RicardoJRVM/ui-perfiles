import { Component, OnInit } from '@angular/core';
import { Modalidad } from '../../../Models/Modalidad';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { MensajeRealizadoComponent } from 'src/app/mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-formulario-modalidad',
  templateUrl: './formulario-modalidad.component.html'
})
export class FormularioModalidadComponent implements OnInit {
  Name:string;
  resultado:any;

  constructor(private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent ) { }

  ngOnInit() {
  }
  ingresar(){
    var texto=(<HTMLInputElement>document.getElementById("NameModality")).value;
    this.Name=texto;
    this.apiRes.createModalidad(this.Name).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
   }
}
