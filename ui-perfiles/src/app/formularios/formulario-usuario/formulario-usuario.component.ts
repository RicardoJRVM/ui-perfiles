import { Component, OnInit } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { MensajeRealizadoComponent } from '../../mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-formulario-usuario',
  templateUrl: './formulario-usuario.component.html'
})
export class FormularioUsuarioComponent implements OnInit {
  email:string;
  password:string;
  resultado:any;
  constructor(public apiRes:ApiPerfilesService,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
  }
  ingresar(){
    var email=(<HTMLInputElement>document.getElementById("email")).value;
    var password=(<HTMLInputElement>document.getElementById("password")).value;
    var names=(<HTMLInputElement>document.getElementById("names")).value;
    var psurname=(<HTMLInputElement>document.getElementById("psurname")).value;
    var msurname=(<HTMLInputElement>document.getElementById("msurname")).value;
    var phone=(<HTMLInputElement>document.getElementById("phone")).value;
    
    this.apiRes.createUsuario(email,password,names,psurname,msurname,phone).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
   }
}
