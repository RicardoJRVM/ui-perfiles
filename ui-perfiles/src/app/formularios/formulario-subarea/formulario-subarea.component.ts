import { Component, OnInit } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { MensajeRealizadoComponent } from 'src/app/mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-formulario-subarea',
  templateUrl: './formulario-subarea.component.html'
})
export class FormularioSubareaComponent implements OnInit {
  Name:string;
  resultado:any;
  constructor(private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent ) { }

  ngOnInit() {
  }
  ingresar(){
    var texto=(<HTMLInputElement>document.getElementById("NameSubArea")).value;
    this.Name=texto;
    this.apiRes.createSubarea(this.Name).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
   }
}
