import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioSubareaComponent } from './formulario-subarea.component';

describe('FormularioSubareaComponent', () => {
  let component: FormularioSubareaComponent;
  let fixture: ComponentFixture<FormularioSubareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioSubareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioSubareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
