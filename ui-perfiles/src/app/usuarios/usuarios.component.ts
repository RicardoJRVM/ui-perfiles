import { Component, OnInit, Inject } from '@angular/core';
import { ApiPerfilesService } from '../api-perfiles.service';
import { Usuario } from '../../Models/Usuario';
import { MatDialog, MatDialogRef ,MAT_DIALOG_DATA} from '@angular/material';
import { Cliente } from '../../Models/Cliente';
import { MensajeRealizadoComponent } from '../mensaje-realizado/mensaje-realizado.component';

export interface DialogData{
  email:string;
  password:string;
}
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html'
})
export class UsuariosComponent implements OnInit {
  usuarios:Cliente[];
  usuario:Cliente;
  resultado:any;
  constructor(private apiRes: ApiPerfilesService,public dialog:MatDialog,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
    this.setUsers();
  }
  setUsers(){
    this.apiRes.getUsuarios().subscribe(
      result => {
        this.usuarios=result.data;
      });
    return this.usuarios;
  }
  buscar_estudiantes(indice:number){
    this.usuario=null;
    while (this.usuario==null) {
      for (let index = 0; index < this.usuarios.length; index++) {
        if(this.usuarios[index].id==indice)
          this.usuario=this.usuarios[index];
      }
    }
    console.log(this.usuario);
    return this.usuario;
     }
  openDialog(indice:number): void {
      this.usuario=this.buscar_estudiantes(indice);
    const dialogRef = this.dialog.open(UsuariosComponentDialog, {
      
      width: '580px',
      data: {usuario:this.usuario}
    });
  }

  eliminar(usuario:Cliente){
    this.apiRes.deleteUsuario(usuario).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
  }
  openDialogEdit(usuario:Cliente): void {
    const dialogRef = this.dialog.open(UsuariosComponentEdit, {
      
      width: '580px',
      data: {usuario: usuario}
    });
  }
  buscar(){
    let word=(<HTMLInputElement>document.getElementById("searchfield")).value;
    this.apiRes.searchclient(word).subscribe(
      result => {
        this.usuarios=result.data;
      });
    return this.usuarios;
}
}
//////////////////////////////////////////////////////
@Component({
  selector: 'app-usuarios-dialog',
  templateUrl: './usuarios.component.dialog.html',
})
export class UsuariosComponentDialog {

  constructor(
    public dialogRef: MatDialogRef<UsuariosComponentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

///////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-usuarios-edit',
  templateUrl: './usuarios.component.dialog.edit.html'
})
export class UsuariosComponentEdit {
  email:any;
  password:any;
  usuario:Cliente;
  resultado:any;
  constructor(
    public dialogRef: MatDialogRef<UsuariosComponentEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  modificar(id:number){
    var email=document.getElementById("email").getAttribute("ng-reflect-model");
   var password=document.getElementById("password").getAttribute("ng-reflect-model");
   var names=document.getElementById("names").getAttribute("ng-reflect-model");
   var psurname=document.getElementById("psurname").getAttribute("ng-reflect-model");
   var msurname=document.getElementById("msurname").getAttribute("ng-reflect-model");
   var phone=document.getElementById("phone").getAttribute("ng-reflect-model");
   this.apiRes.updateUsuario(email,password,names,psurname,msurname,phone,id).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
} 
}