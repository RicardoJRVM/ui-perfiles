import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaCarrerasComponent, ListaCarrerasComponentEdit } from './lista-carreras/lista-carreras.component';
import { ListaModalidadesComponent, ListaModalidadesComponentEdit } from './lista-modalidades/lista-modalidades.component';
import { ListaAreasComponent, ListaAreasComponentEdit } from './lista-areas/lista-areas.component';
import { ListaSubareasComponent, ListaSubareasComponentEdit } from './lista-subareas/lista-subareas.component';
import { MatToolbarModule, MatButtonModule, MatIconModule, MatExpansionModule, MatInputModule, MatListModule, MatCardModule, MatSnackBar, MatSnackBarModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ListaTutorsComponent} from "./lista-tutors/lista-tutors.component";


@NgModule({
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatExpansionModule,
    MatInputModule,
    MatListModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  declarations: [ListaCarrerasComponent, 
    ListaModalidadesComponent,
    ListaAreasComponent,
    ListaTutorsComponent,
    ListaSubareasComponent,
    ListaAreasComponentEdit,
    ListaCarrerasComponentEdit,
    ListaModalidadesComponentEdit,
    ListaSubareasComponentEdit],
    
  exports:[ListaAreasComponent,
    ListaTutorsComponent,
    ListaCarrerasComponent,
    ListaModalidadesComponent,
    ListaSubareasComponent,
    ListaAreasComponentEdit
]
})
export class ListadosModule { }
