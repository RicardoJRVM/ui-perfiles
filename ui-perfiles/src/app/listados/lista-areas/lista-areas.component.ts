import { Component, OnInit, Inject, ViewChild, inject } from '@angular/core';
import { Area } from '../../../Models/Area';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../../usuarios/usuarios.component';
import { MensajeRealizadoComponent } from 'src/app/mensaje-realizado/mensaje-realizado.component';


@Component({
  selector: 'app-lista-areas',
  templateUrl: './lista-areas.component.html'
})
export class ListaAreasComponent implements OnInit {
  areas:Area[];
  area:Area;
  resultado:any;
  constructor(private apiRes: ApiPerfilesService, public dialog:MatDialog, public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
    this.setAreas();
  }
  setAreas(){
    this.apiRes.getAreas().subscribe(
      result => {
        this.areas=result.data;
      });
    return this.areas;
  }  
  eliminar(area:Area){
    this.apiRes.deleteArea(area).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
  }
  openDialog(area:Area): void {
    const dialogRef = this.dialog.open(ListaAreasComponentEdit, {
      
      width: '580px',
      data: {area: area}
    });
  
  }
}
////////////////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-lista-areas-edit',
  templateUrl: './lista-areas.component.edit.html'
})
export class ListaAreasComponentEdit {
  NameArea:any;
  area:Area;
  resultado:any;
  constructor(
    public dialogRef: MatDialogRef<ListaAreasComponentEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  modificar(id:number){
    var texto=document.getElementById("NameArea").getAttribute("ng-reflect-model");
   this.NameArea=texto;
   console.log(id);
   this.apiRes.updateArea(this.NameArea,id).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
}   
verificar(event:any){
  var element=<HTMLInputElement>document.getElementById('submitbut');element.disabled=false;
if(event=="")
{
  var element=<HTMLInputElement>document.getElementById('submitbut');element.disabled=true;
}
}  
}
