import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaModalidadesComponent } from './lista-modalidades.component';

describe('ListaModalidadesComponent', () => {
  let component: ListaModalidadesComponent;
  let fixture: ComponentFixture<ListaModalidadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaModalidadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaModalidadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
