import { Component, OnInit, Inject } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { Modalidad } from '../../../Models/Modalidad';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../../usuarios/usuarios.component';
import { MensajeRealizadoComponent } from 'src/app/mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-lista-modalidades',
  templateUrl: './lista-modalidades.component.html'
})
export class ListaModalidadesComponent implements OnInit {
  modalidades:Modalidad[];
  resultado:any;
  constructor(private apiRes: ApiPerfilesService, public dialog:MatDialog,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
    this.setModalidades();
  }
  setModalidades(){
    this.apiRes.getModalidades().subscribe(
      result => {
        this.modalidades=result.data;
      });
    return this.modalidades;
  }  
  eliminar(modalidad:Modalidad){
    this.apiRes.deleteModalidad(modalidad).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
  }
  openDialog(modalidad:Modalidad): void {
    const dialogRef = this.dialog.open(ListaModalidadesComponentEdit, {
      
      width: '580px',
      data: {modalidad: modalidad}
    });
  
  }
}
//////////////////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-lista-modalidades-edit',
  templateUrl: './lista-modalidades.component.edit.html'
})
export class ListaModalidadesComponentEdit {
  NameModalidad:any;
  modalidad:Modalidad;
  resultado:any;
  constructor(
    public dialogRef: MatDialogRef<ListaModalidadesComponentEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  modificar(id:number){
    var texto=document.getElementById("NameModality").getAttribute("ng-reflect-model");
   this.NameModalidad=texto;
   console.log(id);
   this.apiRes.updateModalidad(this.NameModalidad,id).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
}   
}
