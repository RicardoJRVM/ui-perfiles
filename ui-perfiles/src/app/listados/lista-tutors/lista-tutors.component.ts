import { Component, OnInit, Inject, ViewChild, inject } from '@angular/core';
import { Tutores } from '../../../Models/Tutores';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../../usuarios/usuarios.component';
import { MensajeRealizadoComponent } from '../../mensaje-realizado/mensaje-realizado.component';


@Component({
  selector: 'app-lista-tutors',
  templateUrl: './lista-tutors.component.html'
})
export class ListaTutorsComponent implements OnInit {
  tutors:Tutores[];
  tutor:Tutores;
  resultado:any;
  constructor(private apiRes: ApiPerfilesService, public dialog:MatDialog, public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
    this.setTutors();
  }
  setTutors(){
    this.apiRes.getTutors().subscribe(
      result => {
        this.tutors=result.data;
      });
    console.log(this.tutors)
    return this.tutors;
  }  
  eliminar(tutor:Tutores){
    this.apiRes.deleteTutor(tutor).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
  }
  openDialog(tutor:Tutores): void {
    const dialogRef = this.dialog.open(ListaTutorsComponentEdit, {
      
      width: '580px',
      data: {tutor: tutor}
    });
  
  }
}
//////////////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-lista-tutors-edit',
  templateUrl: './lista-tutors.component.edit.html'
})
export class ListaTutorsComponentEdit {
  nameTutor:any;
  tutor:Tutores;
  resultado:any;
  constructor(
    public dialogRef: MatDialogRef<ListaTutorsComponentEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  modificar(id:number){
    var texto=document.getElementById("nameTutor").getAttribute("ng-reflect-model");
   this.nameTutor=texto;
   console.log(id);
   console.log(this.nameTutor)
   this.apiRes.updateTutor(this.nameTutor,id).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
}
}
