import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSubareasComponent } from './lista-subareas.component';

describe('ListaSubareasComponent', () => {
  let component: ListaSubareasComponent;
  let fixture: ComponentFixture<ListaSubareasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaSubareasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSubareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
