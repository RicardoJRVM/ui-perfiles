import { Component, OnInit, Inject } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { SubArea } from '../../../Models/Subarea';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../../usuarios/usuarios.component';
import { MensajeRealizadoComponent } from 'src/app/mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-lista-subareas',
  templateUrl: './lista-subareas.component.html'
})
export class ListaSubareasComponent implements OnInit {
  subareas:SubArea[];
  resultado:any;
  constructor(private apiRes: ApiPerfilesService,public dialog:MatDialog,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
    this.setSubAreas();
  }
  setSubAreas(){
    this.apiRes.getSubAreas().subscribe(
      result => {
        this.subareas=result.data;
      });
    return this.subareas;
  }  
  eliminar(subarea:SubArea){
    this.apiRes.deleteSubarea(subarea).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
  }
  openDialog(subarea:SubArea): void {
    const dialogRef = this.dialog.open(ListaSubareasComponentEdit, {
      
      width: '580px',
      data: {subarea: subarea}
    });
  
  }
}
//////////////////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-lista-subareas-edit',
  templateUrl: './lista-subareas.component.edit.html'
})
export class ListaSubareasComponentEdit {
  NameSubarea:any;
  subarea:SubArea;
  resultado:any;
  constructor(
    public dialogRef: MatDialogRef<ListaSubareasComponentEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  modificar(id:number){
    var texto=document.getElementById("NameSubArea").getAttribute("ng-reflect-model");
   this.NameSubarea=texto;
   console.log(id);
   this.apiRes.updateSubarea(this.NameSubarea,id).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
}   
}
