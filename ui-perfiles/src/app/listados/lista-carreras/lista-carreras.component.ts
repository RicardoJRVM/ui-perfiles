import { Component, OnInit, Inject } from '@angular/core';
import { ApiPerfilesService } from '../../api-perfiles.service';
import { Carrera } from '../../../Models/Carrera';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { DialogData } from '../../usuarios/usuarios.component';
import { MensajeRealizadoComponent } from 'src/app/mensaje-realizado/mensaje-realizado.component';

@Component({
  selector: 'app-lista-carreras',
  templateUrl: './lista-carreras.component.html'
})
export class ListaCarrerasComponent implements OnInit {
  carreras:Carrera[];
  resultado:any;
  constructor(private apiRes: ApiPerfilesService,public dialog:MatDialog,public mensaje:MensajeRealizadoComponent) { }

  ngOnInit() {
    this.setCareers();
  }
  setCareers(){
    this.apiRes.getCarreras().subscribe(
      result => {
        this.carreras=result.data;
      });
    return this.carreras;
  }  
  eliminar(carrera:Carrera){
    this.apiRes.deleteCarrera(carrera).subscribe(
      result => {
       this.resultado=result.data;
       this.mensaje.openSnackBar();
       window.location.reload();
      });
  }
  openDialog(carrera:Carrera): void {
    const dialogRef = this.dialog.open(ListaCarrerasComponentEdit, {
      
      width: '580px',
      data: {carrera: carrera}
    });
  
  }
}
//////////////////////////////////////////////////////////////////////////////
@Component({
  selector: 'app-lista-carreras-edit',
  templateUrl: './lista-carreras.component.edit.html'
})
export class ListaCarrerasComponentEdit {
  NameCarrera:any;
  carrera:Carrera;
  resultado:any;
  constructor(
    public dialogRef: MatDialogRef<ListaCarrerasComponentEdit>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private apiRes: ApiPerfilesService,public mensaje:MensajeRealizadoComponent) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  modificar(id:number){
    var texto=document.getElementById("NameCareer").getAttribute("ng-reflect-model");
   this.NameCarrera=texto;
   console.log(id);
   this.apiRes.updateCarrera(this.NameCarrera,id).subscribe(
    result => {
     this.resultado=result.data;
     this.mensaje.openSnackBar();
     window.location.reload();
    });
}   
}