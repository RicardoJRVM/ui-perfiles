import { TestBed } from '@angular/core/testing';

import { ApiAreaService } from './api-area.service';

describe('ApiAreaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiAreaService = TestBed.get(ApiAreaService);
    expect(service).toBeTruthy();
  });
});
