import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tutores } from '../Models/Tutores';


@Injectable({
    providedIn: 'root'
})
export class ApiTutorsService {
    public url: string;
    constructor(public http: HttpClient) {
        this.url = 'http://localhost:8000/api/';
    }
    getTutors(): Observable<any> {
        return this.http.get(this.url + 'pruebasss');
    }
}
