import { Area } from "./Area";
import { Carrera } from "./Carrera";
import { SubArea } from "./Subarea";
import { Modalidad } from "./Modalidad";
import { Cliente } from "./Cliente";

export class Perfil {
    id: number;
    tutor: string;

    
    idCarrera: number;

    gestion: string;
    conjunto:boolean;
    cambio:boolean;
    titulo:string;

   
    idArea:number;
    
    
    idModalidad;

    
    idSubarea:number;

    obj_general:string;
    obj_especificos: string;
    descripcion:string;
    director_carrera:string;
    docente_materia:string;
    client_id:number;
    

    constructor(init? : Partial<Perfil>){
        Object.assign(this, init);
    }
    }
    