export class Cliente {
    id: number;
    email:string;
    password:string;
    names:string;
    psurname:string;
    msurname:string;
    phone:number;
    constructor(init? : Partial<Cliente>){
        Object.assign(this, init);
    }
    }